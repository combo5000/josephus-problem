package oml.list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class CircleListTest {

    private CircleList sut;

    @BeforeEach
    void setUp() {
        sut = new CircleList(new LinkedList<>(Arrays.asList(10, 20, 50, 100)));
    }

    @Test
    void initialState() {
        // Assert
        assertThat(sut.getActive(), is(10));
        assertThat(sut.size(), is(4));
    }

    @Test
    void next() {
        // Arrange
        assertThat(sut.getActive(), is(10));
        // Act
        sut.next();
        // Assert
        assertThat(sut.getActive(), is(20));
    }

    @Test
    void nextAtTheEnd() {
        // Arrange
        setState(3, 100);
        assertThat(sut.getActive(), is(100));
        // Act
        sut.next();
        // Assert
        assertThat(sut.getActive(), is(10));
    }

    @Test
    void previous() {
        // Arrange
        assertThat(sut.getActive(), is(10));
        // Act
        sut.previous();
        // Assert
        assertThat(sut.getActive(), is(100));
    }

    @Test
    void remove() {
        // Arrange
        assertThat(sut.getActive(), is(10));
        assertThat(sut.size(), is(4));
        // Act
        sut.remove();
        // Assert
        assertThat(sut.getActive(), is(20));
        assertThat(sut.size(), is(3));
    }

    @Test
    void removeAtTheEnd() {
        // Arrange
        setState(3,100);
        assertThat(sut.getActive(), is(100));
        assertThat(sut.size(), is(4));
        // Act
        sut.remove();
        // Assert
        assertThat(sut.getActive(), is(10));
        assertThat(sut.size(), is(3));
    }

    private void setState(int index, int active) {
        try {
            Field indexField = CircleList.class.getDeclaredField("index");
            Field activeField = CircleList.class.getDeclaredField("active");

            indexField.setAccessible(true);
            activeField.setAccessible(true);

            indexField.set(sut, index);
            activeField.set(sut, active);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            System.out.println(e.getMessage());
        }
    }
}