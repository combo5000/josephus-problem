package oml.list;

import java.util.LinkedList;

public class CircleList {

    private LinkedList<Integer> list;

    private int active;
    private int index;

    public CircleList(LinkedList<Integer> list) {
        this.list = list;
        this.index = 0;
        this.active = list.get(index);
    }

    public int getActive() {
        return active;
    }

    public void next() {
        index++;
        int maxIndex = list.size() - 1;
        if (index > maxIndex) index = 0;
        active = list.get(index);
    }

    public void previous() {
        index--;
        int maxIndex = list.size() - 1;
        if (index < 0) index = maxIndex;
        active = list.get(index);
    }

    public int size() {
        return list.size();
    }

    public void remove() {
        final Integer last = list.getLast();
        final Integer removed = list.remove(index);
        if (last.equals(removed)) index = 0;
        active = list.get(index);
    }
}
