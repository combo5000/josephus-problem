package oml;

import oml.calc.Calculator;
import oml.calc.CalculatorAlgorithm;
import oml.calc.CalculatorArray;
import oml.calc.CalculatorBinary;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new CalculatorArray();
        performCalc(calculator);
        calculator = new CalculatorAlgorithm();
        performCalc(calculator);
        calculator = new CalculatorBinary();
        performCalc(calculator);
    }

    private static void performCalc(Calculator calculator) {
        System.out.println("Performing calculations using : " + calculator.getClass().getSimpleName());
        print(calculator, 1);
        print(calculator, 7);
        print(calculator, 13);
        print(calculator, 16);
        print(calculator, 41);
        print(calculator, 5063);
    }

    private static void print(final Calculator calc, final int x) {
        System.out.printf("Calculating the winner for a circle of %d\n", x);
        long startTime = System.nanoTime();
        final int winner = calc.getWinner(x);
        long totalTime = (System.nanoTime() - startTime) / 1000;
        System.out.printf("The winner is in position: %d\n", winner);
        System.out.printf("The operation took: %d microseconds\n\n", totalTime);
    }
}
