package oml.calc;

import oml.list.CircleList;

import java.util.LinkedList;

public class CalculatorArray implements Calculator {
    @Override
    public int getWinner(int circleSize) {
        //create array with amount of size
        final CircleList circle = createArray(circleSize);
        // remove every next one until one is left
        while (circle.size() > 1) {
            circle.next();
            circle.remove();
        }
        return circle.getActive();
    }

    private CircleList createArray(final int size) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 1; i <= size; i++) {
            list.add(i);
        }
        return new CircleList(list);
    }
}
