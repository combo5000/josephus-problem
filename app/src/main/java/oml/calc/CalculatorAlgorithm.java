package oml.calc;

public class CalculatorAlgorithm implements Calculator {

    @Override
    public int getWinner(final int circleSize) {
        //circleSize= 2^a + l => 2l + 1
        final int a = getBiggestExponentOfTwo(circleSize);
        final int bigValue = (int) Math.pow(2, a);
        final int l = Math.floorMod(circleSize, bigValue);
        return 2 * l + 1;

    }

    private int getBiggestExponentOfTwo(final int circleSize) {
        int value = 2;
        int exponent = 0;
        while (value <= circleSize) {
            value = value * 2;
            exponent++;
        }
        return exponent;
    }
}
