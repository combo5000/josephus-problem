package oml.calc;

public class CalculatorBinary implements Calculator {

    @Override
    public int getWinner(int circleSize) {
        final String binaryString = Integer.toBinaryString(circleSize);
        //add the leading bit to the end
        String resultString = binaryString.substring(1) + binaryString.charAt(0);
        return Integer.parseInt(resultString, 2);
    }
}
