package oml.calc;

public interface Calculator {
    int getWinner(final int circleSize);
}
